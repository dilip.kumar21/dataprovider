package com.datadriven;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class Data {

	@DataProvider(name = "logindata")
	public String[][] getData()throws IOException{
	File f = new File("/home/dilip/Documents/TestData/TestData.xlsx");
	FileInputStream fis = new FileInputStream(f);
	XSSFWorkbook book = new XSSFWorkbook(fis);
	XSSFSheet sheet = book.getSheetAt(0);
	int rows = sheet.getPhysicalNumberOfRows();
	int col =sheet.getRow(0).getLastCellNum();
	String[][] data = new String[rows-1][col];
	for (int i = 0; i < rows-1; i++) {
		for (int j = 0; j <col; j++) {
			DataFormatter d = new DataFormatter();
			data[i][j]=d.formatCellValue(sheet.getRow(i+1).getCell(j));
			
		}
		
	
	}
	return data;
}
}