package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class FetchingMultipleValuesForRegistration {
	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		File f = new File("/home/dilip/Documents/TestData/Testdata.xls");
		FileInputStream fis = new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet sh = book.getSheet("RegisterData");
		// String cellData = sh.getCell(1, 2).getContents();
		// System.out.println("Value Present is " + cellData);
		int rowcount = sh.getRows();
		int columncount = sh.getColumns();
		for (int i = 1; i <rowcount; i++) {
			String FirstName = sh.getCell(0, i).getContents();
			String LastName = sh.getCell(1, i).getContents();
			String Email = sh.getCell(2, i).getContents();
			String Password = sh.getCell(3, i).getContents();
		
			driver.findElement(By.xpath("//a[text()='Register']")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(FirstName);
			driver.findElement(By.id("LastName")).sendKeys(LastName);
			driver.findElement(By.id("Email")).sendKeys(Email);
			driver.findElement(By.id("Password")).sendKeys(Password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(Password);
			driver.findElement(By.id("register-button")).click();
			for (int j = 0; j < columncount; j++) {
				String cell = sh.getCell(j, i).getContents();
				System.out.println("The Input Datas are :" +cell);
			}
		
		}
	}
}
