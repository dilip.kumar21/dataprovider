package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import jxl.read.biff.BiffException;

public class PassingMutlipleValuesforRegUsingApache {

	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		File f = new File("/home/dilip/Documents/TestData/TestData.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet sh = book.getSheetAt(1);

		// String cellData = sh.getCell(1, 2).getContents();
		// System.out.println("Value Present is " + cellData);
		int rowcount = sh.getPhysicalNumberOfRows();

		for (int i = 1; i <rowcount; i++) {
			String FirstName = sh.getRow(i).getCell(0).getStringCellValue();

			String LastName = sh.getRow(i).getCell(1).getStringCellValue();
			String Email = sh.getRow(i).getCell(2).getStringCellValue();
			String Password = sh.getRow(i).getCell(3).getStringCellValue();
			driver.findElement(By.xpath("//a[text()='Register']")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(FirstName);
			driver.findElement(By.id("LastName")).sendKeys(LastName);
			driver.findElement(By.id("Email")).sendKeys(Email);
			driver.findElement(By.id("Password")).sendKeys(Password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(Password);
			driver.findElement(By.id("register-button")).click();

		}

	}
}
